package com.sda.task.controller;

import com.sda.task.service.TaskService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by USER on 2018-11-04.
 */
@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @RequestMapping("/")
    public ModelAndView taskList(ModelAndView modelAndView) {
        modelAndView.addObject("tasks", taskService.getTasks());
        modelAndView.setViewName("taskList");
        return modelAndView;
    }

    @RequestMapping(value = "/task", method = RequestMethod.GET)
    public ModelAndView taskDetails(ModelAndView modelAndView, @RequestParam("id") Long id) {
        modelAndView.addObject("task", taskService.getTask(id));
        modelAndView.setViewName("taskDetails");
        return modelAndView;
    }

}
