<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%--@elvariable id="task" type="com.sda.task.model.Task"--%>
<%--Komentarz--%>
<!DOCTYPE html>
<html>
<jsp:include page="common/head.jsp"/>

<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Lista zadań</h1>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Priority</th>
                    <th scope="col">Deadline</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                    <c:forEach var="task" items="${tasks}" varStatus="loop">
                        <c:set var="rowId" value="${loop.index + 1}"></c:set>
                        <tg:taskRow task="${task}" rowId="${rowId}"/>
                    </c:forEach>
                </tbody>
            </table>

        </div>
    </div>
</div>

</body>
</html>